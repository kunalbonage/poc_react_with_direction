import withDirection, { withDirectionPropTypes } from 'react-with-direction';
import React from 'react';

function ForwardsLabel() {
    return (
        <form>
            First Name: <input type="text" name="FirstName" /><br />
            Last Name: <input type="text" name="LastName" /><br />
            <input type="submit" value="Submit" />
        </form>
    );
}

ForwardsLabel.propTypes = {
    ...withDirectionPropTypes,
};

export default withDirection(ForwardsLabel);