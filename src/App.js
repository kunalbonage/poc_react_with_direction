import React, { Component } from 'react';
import ForwardsLabel from './components/ForwardsLabel';
import DirectionProvider, { DIRECTIONS } from 'react-with-direction/dist/DirectionProvider';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <DirectionProvider direction={DIRECTIONS.RTL}>
          {/* <DirectionProvider direction={DIRECTIONS.LTR}> */}
          <div>
            <ForwardsLabel />
          </div>
        </DirectionProvider>
      </div>
    );
  }
}

export default App;
